/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.itu.tracy.rest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Properties;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import dk.itu.tracy.dsl.GroovyScriptEngine;
import dk.itu.tracy.entity.Entity;
import dk.itu.tracy.facde.AMQPFacade;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * REST Web Service
 *
 * @author sofus
 */
@Path("rest")
public class GenericResource {

    @Context
    private UriInfo context;
    private Properties prop;
    /**
     * Creates a new instance of GenericResource
     * @throws IOException 
     * @throws FileNotFoundException 
     */
    public GenericResource() throws FileNotFoundException, IOException {
    	File file = new File(this.getClass().getClassLoader().getResource("AMQP.properties").getFile());
        prop = new Properties();
        prop.load(new FileReader(file));
        
    }

    /**
     * Retrieves representation of an instance of dk.sal.cphbusiness.jiraplugin.GenericResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public String getJson() throws IOException {
        return "this is not the rest you are looking for";
    }

    
    
 
    @POST
    @Consumes("application/json")
    @Path("jira")
    public void postJsonJira(String data) throws IOException{
        System.out.println(data);
        AMQPFacade facade = new AMQPFacade(prop);
        String dslFile= new String(Files.readAllBytes(new File(this.getClass().getClassLoader().getResource("jira.dsl").getFile()).toPath()));
        List<Entity> ents = GroovyScriptEngine.runDSL(dslFile, data);
        for (Entity entity : ents) {
			facade.sendEvent(entity);
		}
		System.out.println("msg sent");
		facade.close();
    }
    @POST
    @Consumes("application/json")
    @Path("gitlab")
    public void postJsonGitlab(String data) throws IOException{
        System.out.println(data);
        
        AMQPFacade facade = new AMQPFacade(prop);
        String dslFile= new String(Files.readAllBytes(new File(this.getClass().getClassLoader().getResource("gitlab.dsl").getFile()).toPath()));
        List<Entity> ents = GroovyScriptEngine.runDSL(dslFile, data);
		System.out.println("msg parsed");
		for (Entity entity : ents) {
			facade.sendEvent(entity);
		}
		System.out.println("msg sent");
		facade.close();
//        Parser p = new GitLabParser();
//        List<Entity> ents = p.parse(data);
//        for (Entity entity : ents) {
//        	amqp.sendEvent(entity);
//		}
        
    }
}
